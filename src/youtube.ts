import { google } from "googleapis";

const youtube = google.youtube({version: 'v3', auth: process.env.GOOGLE_YOUTUBE_API_KEY});

const getID = (url: string | string[]) => {
    let ID;
    url = (url as string).replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
    if(url[2] !== undefined) {
        ID = url[2].split(/[^0-9a-z_\-]/i);
        ID = ID[0];
    }
    else {
        ID = url;
    }
    return ID;
}

export const getVideoInfo = async (url: string) => {
    const id = getID(url);


    // @ts-ignore
    const video = await youtube.videos.list({
        part: ['snippet'],
        id: [id],
    });
    // @ts-ignore
    const {title, channelTitle} = video.data.items[0].snippet;
    return {title, channel: channelTitle};
}
