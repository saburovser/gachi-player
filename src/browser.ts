import puppeteer from 'puppeteer';

const browser = await puppeteer.launch({
    headless: false,
});
const page = await browser.newPage();

let globalVolume = 100;
export const play = async (url: string) => {
    await page.goto(url, {
        // waitUntil: 'networkidle0',
    });

    await page.waitForSelector('video');
    await page.evaluate(() => {
        const video = document.querySelector('video')!;
        video.play();
        video.volume
    });
    await page.evaluate(async (globalVolume) => {
        const video = document.querySelector('video')!;
        video.volume = globalVolume / 100;

        await new Promise<void>(resolve => {
            video.onended = () => resolve();
        });
    }, globalVolume);
}

export const setVolume = async (volume: number) => {
    if (volume < 0) {
        volume = 0;
    } else if (volume > 100) {
        volume = 100;
    } else if (isNaN(volume)) {
        volume = 50;
    }
    globalVolume = volume;

    return await page.evaluate((volume) => {
        const video = document.querySelector('video')!;
        video.volume = volume / 100;
    }, volume);
}
