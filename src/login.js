await page.goto('https://www.google.com', {
    waitUntil: 'networkidle0',
});

const signin = await page.waitForSelector('a[href*="ServiceLogin"]');
await page.evaluate(el => el.click(), signin);

await page.waitForSelector('input[type=email]');
await page.type('input[type=email]', 'gachi.front@gmail.com', {delay: 20});

const identifierNext = await page.waitForSelector('#identifierNext button');
await page.evaluate(el => el.click(), identifierNext);

await page.waitForSelector('input[name=password]');
await page.waitForTimeout(500);
await page.type('input[name=password]', 'abc12346!', {delay: 20});

const passwordNext = await page.waitForSelector('#passwordNext button');
await page.evaluate(el => el.click(), passwordNext);