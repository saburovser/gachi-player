1. install dependencies with `npm i`
2. compile with `npm run build`
3. start redis with docker `docker run -p 6379:6379 -d redis` or somehow else
4. provide env variables `GOOGLE_YOUTUBE_API_KEY` and `TELEGRAM_BOT_TOKEN`
5. `npm run start`
6. enjoy the gachi!
