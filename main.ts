// Если кто-то придумает, как нормально записывать импорты, чтобы после компиляции они заменялись на такие, то збс
import { play, setVolume } from "./src/browser.js";
import Queue from 'bull';
import { getVideoInfo } from "./src/youtube.js";
import { bot } from "./src/bot.js";
import {User} from 'typegram';

const playQueue = new Queue<{
    url: string,
    user: User,
    title: string,
    channel: string,
}>('song playing');

const regexp = /(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?/gm;

// TODO: придумать нормальную регистрацию, или раскомментить эту, если вдруг нужна
// const allowedUsers: number[] = [];
// bot.use(async (ctx, next) => {
//     const id = ctx.from?.id ?? 0;
//
//     // @ts-ignore
//     const isRegMessage = (ctx.message?.text as string || '').startsWith('/reg');
//
//     if (allowedUsers.includes(id) || isRegMessage) next();
// });
//
// bot.command('reg', ctx => {
//     const text = ctx.message.text;
//     console.log(text);
// })

bot.command('play', async ctx => {
    const text = ctx.message.text;
    const match = text.match(regexp);
    if (!match) return;
    const [url] = match;

    const {title, channel} = await getVideoInfo(url);

    await playQueue.add({
        url,
        user: ctx.message.from,
        title,
        channel,
    });
    const count = await playQueue.getWaitingCount();
    ctx.reply(`Ваш номер в очереди - ${count}`);
});

bot.command('queue', async ctx => {
    const jobs = await playQueue.getWaiting();

    const response = jobs.length
        ? jobs.map((job, i) => `${i + 1}. ${job.data.title} by ${job.data.channel}`).join('\n')
        : 'EMPTY';
    ctx.reply(response);
});

bot.command('now', async ctx => {
    const [job] = await playQueue.getActive();
    if (!job) {
        ctx.reply('Сейчас ничего не играет -_-');
        return;
    }

    const {title, channel} = job.data;
    ctx.reply(`${title} by ${channel}`);
});

bot.command('volume', async ctx => {
    const volume = Number(ctx.message.text.slice(7));
    await setVolume(volume);
});

bot.command('skip', async ctx => {
    const index = Number(ctx.message.text.slice(5));
    if (!index) {
        return;
    } else {
        const jobs = await playQueue.getWaiting();
        if (jobs[index - 1]) {
            await jobs[index - 1].remove();
        }
    }
});

playQueue.process(async job => {
    return await play(job.data.url);
});
